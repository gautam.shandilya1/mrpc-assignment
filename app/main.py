from typing import Union
import sys
from fastapi import FastAPI, Body
from app.exception import CustomException
from app.logg import logging
from app.prediction import predict_paraphrase
from pydantic import BaseModel

app = FastAPI()

class Paraphrase_string(BaseModel):
    sentence1: str
    sentence2: str

@app.get("/")
def root():
    """General function to check api server is working or not"""
    return "Hello World"

@app.post("/predict/", status_code=200)
async def predict(paraphrase_string: Paraphrase_string ):
    """
    Post request required Json data whicj include two keys as follows:
    sentence1: str
    sentence2 : str

    This function will help to call predict_paraphrase function and get result
    
    """
    try:
        res = predict_paraphrase(paraphrase_string.sentence1, paraphrase_string.sentence2)
        return res
    except Exception as e: 
        logging.info("Exception occured in predict function")
        raise CustomException(e, sys)