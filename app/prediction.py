import sys
import ktrain
from app.logg import logging
from app.exception import CustomException


def predict_paraphrase (sen1: str, sen2: str):
    """
    Required input
    sent1: str
    sent2: str 
    this function loads the model and return the prediction result
    """
    try:
        tup = (sen1, sen2)
        mod = ktrain.load_predictor("./app/mrpc")
        res = mod.predict(tup)
    except Exception as e: 
        res = ""
        logging.info("Exception occured in predict paraphrase function")
        raise CustomException(e, sys)
    return res