# MRPC Paraphrase classifier model

### Note: To download data set run download_data.py file seperately

## Steps to run:

1: Download model from gdrive link and add it to /app folder.

2:  Use "docker-compose up" command to build docker image

3: For prediction use "localhost:/predict" in body pass json string like 

{

    sentence1: "String",

    sentence2: "String"
}

4: In response you will get paraphrase or not paraphrase.
