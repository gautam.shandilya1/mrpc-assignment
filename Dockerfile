FROM python:3.9.16-slim

# Creating working directory
WORKDIR /code

# copy requirement file to working directory
COPY ./requirements.txt /code/requirements.txt

# install requirements
RUN pip install --no-cache-dir --upgrade -r requirements.txt

# copy main code to working directoiry
COPY ./app /code/app


# run server
CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "80"]